# README #

This repository contains the Python code to reproduce the experiment in Section 5.2 of 'A near-stationary subspace for ridge approximation' by Constantine, Eftekhari, and Ward. The code requires numpy and scipy, as well as the Python Active subspace Utility Library and Pymanopt. 