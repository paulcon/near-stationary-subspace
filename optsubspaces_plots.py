import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pn
import active_subspaces as asub

def plot_opts(savefigs=True, figtype='.eps'):
    """
    A few options for the plots.

    :param bool savefigs: Save figures into a separate figs director.
    :param str figtype: A file extention for the type of image to save.

    :return: opts, The chosen options. The keys in the dictionary are
        `figtype`, `savefigs`, and `font`. The `font` is a dictionary that
        sets the font properties of the figures.
    :rtype: dict
    """

    # make figs directory
    if savefigs:
        if not os.path.isdir('figs'):
            os.mkdir('figs')

    # set plot fonts
    myfont = {'family' : 'arial',
            'weight' : 'normal',
            'size' : 14}

    opts = {'figtype' : figtype,
            'savefigs' : savefigs,
            'myfont' : myfont}

    return opts
    
def plot_sigs():
    # get X, f, and df
    df = pn.DataFrame.from_csv('SU2-NACA0012.txt')
    data = df.as_matrix()
    maxM = 201
    X = data[1:maxM,:18]/0.01
    df_drag = data[1:maxM,38:]
    
    M, m = X.shape
    df = df_drag.reshape((M, m))
    
    ss = asub.subspaces.Subspaces()
    ss.compute(df=df, sstype=0, nboot=100)
    e = ss.eigenvalues[:10,:]
    e_br = ss.e_br[:10,:]
    sub_br = ss.sub_br[:10,:]
    
    k = e.shape[0]
    plt.figure(figsize=(5,5))
    plt.rc('font', **opts['myfont'])
    plt.semilogy(range(1 ,k+1), e, 'ko-',markersize=12,linewidth=2)
    plt.fill_between(range(1, k+1), e_br[:,0], e_br[:,1],
        facecolor='0.7', interpolate=True)
    plt.xlabel('Index')
    plt.ylabel('Eigenvalues')
    plt.grid(True)
    plt.xticks(range(1, k+1))
    plt.axis([0, k+1, 1e-3, 10])

    if opts['savefigs']:
        figname = 'figs/evals_drag' + opts['figtype']
        plt.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0.01)
    
    kk = sub_br.shape[0]
    plt.figure(figsize=(5,5))
    plt.rc('font', **opts['myfont'])
    plt.semilogy(range(1, kk+1), sub_br[:,1], 'ko-', markersize=12)
    plt.fill_between(range(1, kk+1), sub_br[:,0], sub_br[:,2],
        facecolor='0.7', interpolate=True)
    plt.xlabel('Subspace dimension')
    plt.ylabel('Subspace distance')
    plt.grid(True)
    plt.xticks(range(1, kk+1))
    plt.axis([0, kk+1, 1e-2, 1])

    if opts['savefigs']:
        figname = 'figs/subspace_drag' + opts['figtype']
        plt.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0.01)

if __name__ == '__main__':
    
    plt.close('all')
    opts = plot_opts()
    
    plot_sigs()
    """
    for N in range(2,6):
            
        d = np.load('experiment_N{:d}.npz'.format(N))
        
        id_times = d['id_times']
        as_times = d['as_times']
        rn_times = d['rn_times']
        
        id_rvecs = d['id_rvecs']
        as_rvecs = d['as_rvecs']
        rn_rvecs = d['rn_rvecs']
        
        iterz = range(1,11)
        fsize = 2.5
        
        for n in range(1,5):
            plt.figure(figsize=(fsize,fsize))
            plt.rc('font', **opts['myfont'])
            plt.semilogy(iterz, rn_rvecs[:,n-1,0], 'b--', label='rand, {:4.0f}s'.format(np.mean(rn_times[n-1,:])))
            plt.semilogy(iterz, rn_rvecs[:,n-1,1:], 'b--')
            plt.semilogy(iterz, id_rvecs[:,n-1], 'ko-', label='id, {:4.0f}s'.format(id_times[n-1]), linewidth=2, markersize=10)
            plt.semilogy(iterz, as_rvecs[:,n-1], 'ro-', label='as, {:4.0f}s'.format(as_times[n-1]), linewidth=2, markersize=10)
            #plt.xlabel('Iteration')
            #plt.ylabel('Residual')
            #plt.title('N={:d}, n={:d}'.format(N,n))
            plt.axis([1, 10, 1e-3, 1e-1])
            plt.grid(True)
            #plt.legend()
            plt.show()
            
            if opts['savefigs']:
                figname = 'figs_sm/res_n{:d}_N{:d}{}'.format(n,N,opts['figtype'])
                plt.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0.05)
                
        #fig, ax = plt.figure(figsize=(fsize,fsize))
        fig, ax = plt.subplots(figsize=(5,5))
        ind = np.arange(3)
        width = 0.2
        
        tts = (np.mean(rn_times[0,:]), id_times[0], as_times[0])
        ax.bar(ind, tts, width, color='k')
        
        tts = (np.mean(rn_times[1,:]), id_times[1], as_times[1])
        ax.bar(ind+width, tts, width, color='r')
        
        tts = (np.mean(rn_times[2,:]), id_times[2], as_times[2])
        ax.bar(ind+2*width, tts, width, color='b')
        
        tts = (np.mean(rn_times[3,:]), id_times[3], as_times[3])
        ax.bar(ind+3*width, tts, width, color='g')
            
        # add some text for labels, title and axes ticks
        ax.set_ylabel('Wallclock time (s)')
        ax.set_xticks(ind + 2*width)
        ax.set_xticklabels(('RN', 'ID', 'AS'))
        
        ax.set_ylim((0, 450))
        
        ax.legend(('n=1', 'n=2', 'n=3', 'n=4'))
        
        
        plt.show()
            
        if opts['savefigs']:
            figname = 'figs_sm/times_N{:d}{}'.format(N,opts['figtype'])
            plt.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0.05)
    """