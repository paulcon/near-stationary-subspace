import numpy as np
import pandas as pn
from active_subspaces.utils.response_surfaces import PolynomialApproximation

from pymanopt.manifolds import Grassmann
from pymanopt import Problem
from pymanopt.solvers import SteepestDescent

import time

class RidgeApproximation():
    
    U = None
    respsurf = None
    
    def train(self, X, f, U0, N, maxiter, tol):
        
        # Instantiate the polynomial approximation
        rs = PolynomialApproximation(N=N)
        
        # Instantiate the Grassmann manifold        
        m, n = U0.shape
        manifold = Grassmann(m, n)
        
        # Alternating minimization
        i = 0
        res = 1e9
        resvecs = np.zeros((maxiter, ))
        while i < maxiter and res > tol:
            
            # Train the polynomial approximation with projected points
            Y = np.dot(X, U0)
            rs.train(Y, f)
            
            # Minimize residual with polynomial over Grassmann
            func = lambda y: _res(y, X, f, rs)
            grad = lambda y: _dres(y, X, f, rs)
            
            problem = Problem(manifold=manifold, cost=func, egrad=grad, verbosity=0)
            solver = SteepestDescent(logverbosity=0)
            U1 = solver.solve(problem, x=U0)
            
            # Evaluate and store the residual
            res = func(U1)
            resvecs[i] = res
            
            # Update iterators
            U0 = U1
            i += 1
            
        # Store data
        self.U = U1
        self.respsurf = rs
        
        if i==maxiter:
            exitflag = 1
        else:
            exitflag = 0
            
        return exitflag, resvecs
        
    def predict(self, X, compgrad=False):

        Y = np.dot(X, self.U)
        f, dfdy = self.respsurf.predict(Y, compgrad)
        if compgrad:
            dfdx = np.dot(dfdy, self.U.transpose())
        else:
            dfdx = None
        return f, dfdx

def _res(U, X, f, rs):
    M, m = X.shape
    Y = np.dot(X, U)
    g = rs.predict(Y)[0]
    res = 0.5*np.linalg.norm(f - g)**2
    return res
    
def _dres(U, X, f, rs):
    M, m = X.shape
    n = U.shape[1]
    Y = np.dot(X, U)
    g, dg = rs.predict(Y, compgrad=True)
    dR = np.zeros((m, n))
    for i in range(M):
        dR += (g[i,0] - f[i,0])*np.dot(X[i,:].reshape((m, 1)), dg[i,:].reshape((1, n)))
    return dR
    
if __name__ == '__main__':
    
    # get X, f, and df
    df = pn.DataFrame.from_csv('SU2-NACA0012.txt')
    data = df.as_matrix()
    maxM = 201
    X = data[1:maxM,:18]/0.01
    f_lift = data[1:maxM,18]
    f_drag = data[1:maxM,19]
    df_lift = data[1:maxM,20:38]
    df_drag = data[1:maxM,38:]
    
    M, m = X.shape
    f = f_drag.reshape((M, 1))
    df = df_drag.reshape((M, m))
    
    W, sig, V = np.linalg.svd(df_drag, full_matrices=0)
    V = V.transpose()
    
    ra = RidgeApproximation()
    maxiter = 5
    
    maxdeg = 2
    maxn = 2
    numrn = 3
    
    id_times = np.zeros((maxdeg, maxn))
    as_times = np.zeros((maxdeg, maxn))
    rn_times = np.zeros((maxdeg, maxn, numrn))
    
    id_rvecs = np.zeros((maxiter, maxdeg, maxn))
    as_rvecs = np.zeros((maxiter, maxdeg, maxn))
    rn_rvecs = np.zeros((maxiter, maxdeg, maxn, numrn))
    
    # loop over polynomial degree
    for N in range(1, maxdeg+1):
        
        # loop over dimension of active space
        for n in range(1, maxn+1):
            
            # identity starting point
            U0 = np.eye(m, n)
                    
            # repeat training 10 times for timing
            tt = np.zeros((10, ))
            for k in range(10):
                t0 = time.time()
                ef, rvecs = ra.train(X, f, U0, N, maxiter, 1e-10)
                tt[k] = time.time() - t0
                
            id_times[N-1, n-1] = np.mean(tt)
            id_rvecs[:, N-1, n-1] = rvecs
            
            
            # active subspace starting point
            U0 = V[:,:n]
                    
            # repeat training 10 times for timing
            tt = np.zeros((10, ))
            for k in range(10):
                t0 = time.time()
                ef, rvecs = ra.train(X, f, U0, N, maxiter, 1e-10)
                tt[k] = time.time() - t0
                
            as_times[N-1, n-1] = np.mean(tt)
            as_rvecs[:, N-1, n-1] = rvecs
            
            # random starting points
            for i in range(numrn):
                U0 = np.linalg.qr(np.random.normal(size=(m, n)))[0]
                
                # repeat training 10 times for timing
                tt = np.zeros((10, ))
                for k in range(10):
                    t0 = time.time()
                    ef, rvecs = ra.train(X, f, U0, N, maxiter, 1e-10)
                    tt[k] = time.time() - t0
                    
                rn_times[N-1, n-1, i] = np.mean(tt)
                rn_rvecs[:, N-1, n-1, i] = rvecs
                
            print 'n: {:d}'.format(n)
        print 'N: {:d}'.format(N)
            
    np.savez('experiment', id_times=id_times, id_rvecs=id_rvecs, \
            as_times=as_times, as_rvecs=as_rvecs, \
            rn_times=rn_times, rn_rvecs=rn_rvecs)
        
    
    
    
    
    
    
    